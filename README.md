# Theme for presentations

This theme is based on [reveal.js](https://revealjs.com/) and adapted for presentations given under the R3 umbrella.

## How to get started

Include this theme as a submodule next to your presentation.

## Additions compared to plain `reveal.js`

- [MathJax](https://www.mathjax.org/) is enabled
- [FontAwesome icons](https://fontawesome.com/icons) are included by default,
  use e.g. `<i class="fa-regular fa-acorn"></i>` in the markdown to utilize
  them
- [AmazingTwemoji](https://github.com/SebastianAigner/twemoji-amazing) is
  included by default, use similarly with
  `<i class="twa twa-face-with-monocle">`
- To make a huge text (e.g., for separating long series of slides), use the
  `.leader` class, e.g., `<div class=leader>A new section</div>` for a separate
  slide.
